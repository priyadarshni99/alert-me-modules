import { RoutePath } from "./RoutePath";
export const header =[
    {
        name:"Dashboard",
        path:RoutePath.DASHBOARD
    },
    {
        name:"Individual Users",
        path:RoutePath.INDIVIDUAL_USERS
    },
    {
        name:"Organisation Users",
        path:RoutePath.ORGANISATION_USERS
    },
    {
        name:"Plans",
        path:RoutePath.PLANS
    },
    {
        name:"Notifications",
        path:RoutePath.NOTIFICATIONS
    },
    {
        name:"Promotions",
        path:RoutePath.PROMOTIONS
    },
    {
        name:"Approvals",
        path:RoutePath.APPROVALS
    }
];