export const RoutePath ={
    DASHBOARD:"/dashboard",
    INDIVIDUAL_USERS:"/individualUsers",
    ORGANISATION_USERS:"/organisationUsers",
    PLANS:"/plans",
    NOTIFICATIONS:"/notifications",
    PROMOTIONS:"/promotions",
    APPROVALS:"/approvals"
};