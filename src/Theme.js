import { createMuiTheme } from '@material-ui/core/styles';

const Theme = createMuiTheme({
    palette:{
    },
    typography:{
        headerFontFamily:"'Rubik', sans-serif",
        fontFamily:"'Roboto', sans-serif"
    },
    colors:{
        tableHeader_color:"#D2E1FC",
        white:"#FFFFFF",
        headerFont:"#FFFFFFA3",
        header:"#2147AC",
        toolbar:"#2C56C6",
        grey:"#0000008A",
        backgroundColor:"#F4F4F4",
        tableFooter:"#F8FCFF",
        tableHeaderText:"#0000008A"
    },
    fontSize:{
    }
    
  });

  export default Theme;