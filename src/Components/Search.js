
import { makeStyles } from "@material-ui/styles";
import React, { useState } from "react";
const useStyles = makeStyles({
  input1: {
    height: 40,
    width: "240px",
    height: "42px",
    padding: "0px",
    marginLeft: "350px",
    marginTop: "14px",
  },
});
const Search = (props) => {
  const styles = useStyles();
  const [state, setState] = useState({
    search: null,
  });
  const handleChange = (e) => {
    setState({ search: e.target.value });
  };
  const handleClick = (e) => {
    if (e.key === "Enter") {
    }
  };

  return (
    <>

      <div
        style={{
          height: "42px",
          width: "240px",
          backgroundColor: "#F4F4F4",
          color: "#0000008A",
          borderRadius: "3px",
          paddingLeft: "10px",
          marginLeft:"500px"
        }}
      >
        <i class="fa fa-search icon" style={{fontSize:"16px"}}></i>
        <input
          type="text"
          style={{
            height: "42px",
            backgroundColor: "#F4F4F4",
            color: "#0000008A",
            fontSize: "14px",
            paddingLeft: "10px",
            border: "none",
            outline:"none",
            fontFamily:"'Rubik', sans-serif"
          }}
          placeholder="Search"
          onKeyPress={handleClick}
          onChange={handleChange}
        />
      </div>
    </>
  );
};

export default Search;

