import { AppBar, makeStyles, Toolbar, Typography } from "@material-ui/core";
import React from "react";
import { NavLink } from "react-router-dom";
import { header } from "../Constants/Header";
const useStyles = makeStyles((theme) => ({
  appbar: {
    position:"static",
    height: "64px",
    paddingLeft: "64px",
    backgroundColor: theme.colors.appbar,
  },
  toolbar: {
    backgroundColor: theme.colors.toolbar,
  },
  link: {
    height: "64px",
    color: theme.colors.headerFont,
    fontSize: "16px",
    textDecoration: "none",
    fontFamily: theme.typography.headerFontFamily,
    marginLeft: "20px",
    marginRight: "20px",
    paddingTop: "22px",
  },
  activeLink: {
    color: theme.colors.white,
    alignItems:"center"
  },
  borderBottom: {
    height: "1px",
    backgroundColor: "white",
    marginTop: "17px",
    width: "30px",
    paddigLeft: "15%",
  },
}));
export default function Header(props) {
  const styles = useStyles();
 
  return (
    <>
      <AppBar className={styles.appbar}>
        <Toolbar className={styles.toolbar}>
          {header.map((item) => {
            
            return (
              <NavLink
                className={styles.link}
                activeClassName={styles.activeLink}
                to={item.path}
              >
                {item.name}
              </NavLink>
            );
          })}
        </Toolbar>
      </AppBar>
    </>
  );
}
