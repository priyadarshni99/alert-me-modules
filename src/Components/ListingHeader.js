import { AppBar, Box, Button, Paper, Toolbar, Typography } from "@material-ui/core";
import React from "react";
import Search from "./Search";
import FilterListIcon from "@material-ui/icons/FilterList";
import GetAppIcon from "@material-ui/icons/GetApp";
import { getThemeProps } from "@material-ui/styles";
const useStyles = (theme) => ({
  container: {
    width: "100%",
    height: "74px",
    backgroundColor: "#FFFFFF",
    paddingTop: "16px",
    marginLeft: "0px",
  },
  toolbar: { 
    width: "100%",
    padding: "0",
    backgroundColor: "#FFFFFF",
  },
  title: {
    fontSize: "20px",
    marginLeft: "64px",
    color: "black",
    width: "124px",
    fontWeight: "Bold",
    marginLeft: "64px",
  },
  filter: {
    width: "98px",
    height: "42px",
    marginLeft: "12px",
    backgroundColor: "#E2E2E2",
  },
});
export default function ListingHeader(props) {
  const styles = useStyles();
  return (
    <>
      <Paper className={styles.container}>
        <Toolbar className={styles.toolbar}>
          <Typography>
            <Box
              fontWeight="fontWeightBold"
              fontSize={20}
              style={{ marginLeft: "64px" }}
            >
              {props.title}
            </Box>
          </Typography>
          <Search />
          <Button
            style={{
              backgroundColor: "#E2E2E2",
              color: "#000000",
              height: "42px",
              marginLeft: "16px",
              fontSize: "14px",
              fontFamily: "'Rubik', sans-serif",
            }}
          >
            <FilterListIcon />
            Filter
          </Button>
          <Button
            style={{
              backgroundColor: "#59B961",
              color: "#FFFFFF",
              height: "42px",
              marginLeft: "16px",
              fontSize: "14px",
              fontFamily: "'Rubik', sans-serif",
            }}
          >
            NEW
          </Button>
          <button
            style={{
              backgroundColor: "#264DB5",
              border: "none",
              color: "#FFFFFF",
              height: "42px",
              width: "42px",
              borderRadius: "5px",
              marginLeft: "16px",
            }}
          >
            <GetAppIcon />
          </button>
        </Toolbar>
      </Paper>
    </>
  );
}
