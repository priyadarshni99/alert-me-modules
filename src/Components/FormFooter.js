import { Button, Paper, Toolbar } from "@material-ui/core";
import React from "react";

export default function FormFooter() {
  return (
    <>
      <Paper
        styles={{
            marginBottom:"0px",
          position: "static",
          bottom: "0px",
          height: "64px",
          width: "100%",paddingLeft:"500px"
        }}
      >
        <Toolbar styles={{marginLeft:"500px"}}>
          <Button
            styles={{
              height: "42px",
              fontSize: "14px",
              fontFamily: "'Rubik', sans-serif",
              backgroundColor: "#1976D2",
              color: "#FFFFFF",
            }}
          >
            Cancel
          </Button>
          <Button
            style={{
              backgroundColor: "#1976D2",
              color: "#FFFFFF",
              height: "42px",
              marginLeft: "16px",
              fontSize: "14px",
              fontFamily: "'Rubik', sans-serif",
            }}
          >
            SAVE
          </Button>
        </Toolbar>
      </Paper>
    </>
  );
}
