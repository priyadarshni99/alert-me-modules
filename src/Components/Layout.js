import { ThemeProvider } from '@material-ui/styles';
import React from 'react';
import Listing from '../Modules/Notifications/Listing';
import Theme from '../Theme';
import FormFooter from './FormFooter';
import Header from './Header';
import ListingHeader from './ListingHeader';

export default function Layout(props) {
  console.log("Layout ",props);
  return (
    <>
    <ThemeProvider theme={Theme}>
        <Header/>
        <ListingHeader title="Notifications"/>
        <Listing/>
        <FormFooter/>
    </ThemeProvider>
    </>
  );
}
