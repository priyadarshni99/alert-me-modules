
import { makeStyles } from "@material-ui/core";
import Layout from "./Components/Layout";

function App() {
  return (
    <div style={{backgroundColor:"#F4F4F4"}}>
      <Layout/>
    </div>
  );
}

export default App;
