import React from 'react';
import TableComponent from '../../Components/TableComponent';

export default function Listing() {
    const columns = [
        { id: "productName", label: "Product Name", minWidth: 150 },
        { id: "title", label: "Title", minWidth: 150 },
        { id: "description", label: "Description", minWidth: 150 },
        { id: "planType", label: "Plan Type", minWidth: 150 },
        { id: "notifyAt", label: "Notify At", minWidth: 150 },
        { id: "notifyUser", label: "Notify User", minWidth: 150 }
      ];
      const rows=[{
          productName:"dfhvjls",
          title:"sdfjkla",
          description:"sjkfh",
          planType:"ahsfujuisd",
          notifyAt:"ejfhij",
          notifyUser:"esgfijs"
      }]
  return (
    <>
    <TableComponent columns={columns} rows={rows} link="/"/>
    </>
  );
}
